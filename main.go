package main

//imported dependencies
import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"reflect"
	"io/ioutil"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

//Project struct containing the most basic information
type Project struct {
	Project        string                               `json:"name"`
	Owner          struct{ Name string `json:"login"` } `json:"owner"`
	TopContributor string
	Commits        int
}

//Contributor struct containing the amount of commits etc
type Contributor struct {
	Name    string `json:"login"`
	Commits int    `json:"contributions"`
}

//Languages struct containing the various languages
type Languages struct {
	Keys map[string]interface{} `json:"-"`
}

//ResponsePayload returns the information requested to the user
type ResponsePayload struct {
	Project        string   `json:"name"`
	Owner          string   `json:"owner"`
	TopContributor string
	Commits        int
	Languages      []string `json:"language"`
}

func init() { //"Main" for Google Cloud
	//func main() { //For localhost only, does not work on Google Cloud
	http.HandleFunc("/", handlerWrongURL)           //Default URL, there in case the user enters an invalid URL
	http.HandleFunc("/projectinfo/v1/", handlerURL) //Basis URL for the program
	//http.ListenAndServe(":8080", nil) //For localhost
}

//Default URL, there in case the user enters an invalid URL
func handlerWrongURL(w http.ResponseWriter, r *http.Request) {
	httpError(w, 400, "The URL format was incorrect! Please use /projectinfo/v1/github.com/[user]/[project]")
}

//Basis URL for the program
func handlerURL(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") //Splits the URL into parts
	//fmt.Fprintln(w, len(parts)) //debug
	if (len(parts) == 6 && parts[5] != "") || (len(parts) == 7 && parts[6] == "") { //Checks whether the URL has a valid amount of parts
		projectURL := parts[4] + "/" + parts[5]             //Creates the URL part for the project
		url := "https://api.github.com/repos/" + projectURL //The URL from which information is retrieved

		ctx := appengine.NewContext(r) //Google Cloud app engine context
		client := urlfetch.Client(ctx) //Client for fetchingURLs
		page, error := client.Get(url) //Retrieve the page and potential error messages
		if error != nil || page.StatusCode != 200 { //If page encountered an error
			httpError(w, 404, "The URL format was correct, but the page ("+url+") could not be found or loaded properly")
			return
		}                              //If the page with the basic URL has been found and is working

		var project Project            //Basic project info
		var contributors []Contributor //List of contributors
		var languages Languages        //Languages

		json.NewDecoder(page.Body).Decode(&project) //Decode the info obtained from the webpage

		//Obtain info about contributors
		page, error = client.Get(url + "/contributors") //Retrieve the page and potential error messages
		if error != nil || page.StatusCode != 200 { //If page encountered an error
			httpError(w, 404, "The URL format was correct, but the page ("+url+") could not be found or loaded properly")
			return
		}
		json.NewDecoder(page.Body).Decode(&contributors) //Decode the info obtained from the webpage

		//Obtain info about languages
		page, error = client.Get(url + "/languages") //Retrieve the page and potential error messages
		if error != nil || page.StatusCode != 200 { //If page encountered an error
			httpError(w, 404, "The URL format was correct, but the page ("+url+") could not be found or loaded properly")
			return
		}
		body, error := ioutil.ReadAll(page.Body) //Retrieve the page and potential error messages
		if error = json.Unmarshal([]byte(body), &languages.Keys); error != nil { //If page encountered an error, also retrieves languages
			httpError(w, 401, "Could not retrieve languages.")
			return
		}

		json.NewEncoder(w).Encode(buildResponse(project, contributors[0], languages)) //Encodes all info into a json response

	} else { //If the URL had an incorrect format
		httpError(w, 400, "The URL format was incorrect! Please use /projectinfo/v1/github.com/[user]/[project]")
	}
}

//Builds the json response
func buildResponse(p Project, c Contributor, l Languages) ResponsePayload {
	var r ResponsePayload                     //Response object
	r.Project = p.Project                     //Project name
	r.Owner = p.Owner.Name                    //Owner name
	r.TopContributor = c.Name                 //Top contributor
	r.Commits = c.Commits                     //Amount of commits
	keys := reflect.ValueOf(l.Keys).MapKeys() //keys
	for _, elem := range keys { //for each key
		r.Languages = append(r.Languages, elem.Interface().(string)) //add language to languages
	}
	return r
}

//Function for displaying http error with a standardized format
func httpError(w http.ResponseWriter, code int, text string) {
	http.Error(w, strconv.Itoa(code)+": "+http.StatusText(code)+"\n"+text+"\n", code)
}
